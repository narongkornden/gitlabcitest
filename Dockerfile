FROM openjdk:8u111-jdk-alpine
VOLUME /tmp
ADD /target/*.jar  app.jar

ENV TZ 'Asia/Bangkok'
ENV JAVA_OPTS="-Xms128m -Xmx512m"

#ENTRYPOINT ["java","$JAVA_OPTS -Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]

ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar app.jar" ]


